import java.awt.Container;
import java.awt.FontMetrics;
import java.awt.GridLayout;
import java.awt.event.*;
import java.text.BreakIterator;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.SwingUtilities;

import CLIPSJNI.Environment;
import CLIPSJNI.FactAddressValue;
import CLIPSJNI.MultifieldValue;
import CLIPSJNI.PrimitiveValue;
import CLIPSJNI.SymbolValue;

public class DiagnozowanieStomatologiczne implements ActionListener 
{
	  		  /*=============================*/
		      /*           Interface         */
		      /*=============================*/
	   JLabel displayLabel;
	   JButton nextButton;
	   JPanel choicesPanel;
	   ButtonGroup choicesButtons;
		
	   ResourceBundle DiagnozowanieStomatologiczneResources; 
	   Environment clips;
	   String relationAsserted;
	   String lastRelationAsserted="";
	  
	
	DiagnozowanieStomatologiczne(){
		  
		      try
		        {
			DiagnozowanieStomatologiczneResources = ResourceBundle.getBundle("DiagnozowanieStomatologiczneResources",Locale.getDefault());
		        }
		      catch (MissingResourceException mre)
		       {
		         mre.printStackTrace();
		         return;
		        }
		      /*================================*/
		      /* Create a new JFrame container. */
		      /*================================*/
		     
		      JFrame jfrm = new JFrame(DiagnozowanieStomatologiczneResources.getString("DiagnozowanieStomatologiczne"));  
  

		      /*=============================*/
		      /* Specify FlowLayout manager. */
		      /*=============================*/
		        
		      jfrm.getContentPane().setLayout(new GridLayout(3,1));  
		 
		      /*=================================*/
		      /* Give the frame an initial size. */
		      /*=================================*/
		     
		      jfrm.setSize(650,350);  
		  
		      /*=============================================================*/
		      /* Terminate the program when the user closes the application. */
		      /*=============================================================*/
		     
		      jfrm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);  
		 
		      /*===========================*/
		      /* Create the display panel. */
		      /*===========================*/
		      
		      JPanel displayPanel = new JPanel(); 
		      displayLabel = new JLabel();
		      displayPanel.add(displayLabel);
		      
		      /*===========================*/
		      /* Create the choices panel. */
		      /*===========================*/
		     
		      choicesPanel = new JPanel(); 
		      choicesButtons = new ButtonGroup();
		      
		      /*===========================*/
		      /* Create the buttons panel. */
		      /*===========================*/

		      JPanel buttonPanel = new JPanel(); 
		      

		      nextButton = new JButton(DiagnozowanieStomatologiczneResources.getString("Next"));
		      nextButton.setActionCommand("Next");
		      buttonPanel.add(nextButton);
		      nextButton.addActionListener(this);
		     
		      /*=====================================*/
		      /* Add the panels to the content pane. */
		      /*=====================================*/
		      
		      jfrm.getContentPane().add(displayPanel); 
		      jfrm.getContentPane().add(choicesPanel); 
		      jfrm.getContentPane().add(buttonPanel); 

		      /*========================*/
		      /* Load the auto program. */
		      /*========================*/
		      
		      clips = new Environment();
		      
		      clips.load("DiagnozowanieStomatologiczne.clp");
		      
		      clips.reset();
		      clips.run(1);

		      /*====================*/
		      /* Display the frame. */
		      /*====================*/
		      
		      jfrm.setVisible(true);  

		      /*==================================*/
		      /* Get the current state of the UI. */
		      /*==================================*/
		      
		      nextUIState();
		     }  	      
		      	
	private void nextUIState()  {
		String evalStr = "(find-all-facts ((?f UI-state)) TRUE)";
		
	      
		MultifieldValue pv = (MultifieldValue) clips.eval(evalStr);
	      
		int tNum = pv.listValue().size();
	      
	      if (tNum == 0) {System.out.println("Nie ma wiecej UI");return;}

	      FactAddressValue fv = (FactAddressValue) pv.listValue().get(0);
	      
	      relationAsserted = fv.getFactSlot("relation-asserted").toString();
	     
	  
	      if (relationAsserted.contentEquals(lastRelationAsserted)) 
	      		{
	    	  	clips.run(1);
	    	  	nextUIState();
	    	  	return;
	      		}
	      
	      lastRelationAsserted = relationAsserted;
	      
	      /*========================================*/
	      /*    Determine the Next button states.   */
	      /*========================================*/
	      
	      if (fv.getFactSlot("state").toString().equals("final"))
	        { 
	         nextButton.setActionCommand("Restart");
	         nextButton.setText(DiagnozowanieStomatologiczneResources.getString("Restart")); 
	        }
	      else if (fv.getFactSlot("state").toString().equals("initial"))
	        {
	         nextButton.setActionCommand("Next");
	         nextButton.setText(DiagnozowanieStomatologiczneResources.getString("Next"));
	        }
	      else
	        { 
	         nextButton.setActionCommand("Next");
	         nextButton.setText(DiagnozowanieStomatologiczneResources.getString("Next"));
	        }
	      
	      /*=====================*/
	      /* Set up the choices. */
	      /*=====================*/
	      
	      choicesPanel.removeAll();
	      choicesButtons = new ButtonGroup();
	            
	      pv = (MultifieldValue) fv.getFactSlot("valid-answers");
	      
	      List theList = pv.listValue();
	      
	      String selected = fv.getFactSlot("response").toString();
	     
	      for (Iterator itr = theList.iterator(); itr.hasNext();) 
	        {
	         PrimitiveValue bv = (PrimitiveValue) itr.next();
	         JRadioButton rButton;
	                        
	         if (bv.toString().equals(selected))
	            { rButton = new JRadioButton(DiagnozowanieStomatologiczneResources.getString(bv.toString()),true); }
	         else
	            { rButton = new JRadioButton(DiagnozowanieStomatologiczneResources.getString(bv.toString()),false); }
	                     
	         rButton.setActionCommand(bv.toString());
	         choicesPanel.add(rButton);
	         choicesButtons.add(rButton);
	        }
	        
	      choicesPanel.repaint();
	      
	      /*====================================*/
	      /* Set the label to the display text. */
	      /*====================================*/

	      String theText = DiagnozowanieStomatologiczneResources.getString(((SymbolValue) fv.getFactSlot("display")).stringValue());
	            
	      wrapLabelText(displayLabel,theText);
	      
	} 
	@Override
	public void actionPerformed(ActionEvent event) {

	      /*=========================*/
	      /* Handle the Next button. */
	      /*=========================*/
	      
	      if (event.getActionCommand().equals("Next"))
	        {
	         if (choicesButtons.getButtonCount() != 0)
	           {
	            clips.assertString("(" +relationAsserted+" "+choicesButtons.getSelection().getActionCommand()+")");
	           System.out.println("(" +relationAsserted+" "+choicesButtons.getSelection().getActionCommand()+")");
	           }
	         else
	         {
	        	 clips.assertString( "(start)" );
	         }
	           
	         clips.run(1);
	         nextUIState();
	        }
	      else if (event.getActionCommand().equals("Restart"))
	        { 
	         clips.reset(); 
	         clips.run(1);
	         nextUIState();
	        }
	     }

	 private void wrapLabelText(
		     JLabel label, 
		     String text) 
		     {
		      FontMetrics fm = label.getFontMetrics(label.getFont());
		      Container container = label.getParent();
		      int containerWidth = container.getWidth();
		      int textWidth = SwingUtilities.computeStringWidth(fm,text);
		      int desiredWidth;

		      if (textWidth <= containerWidth)
		        { desiredWidth = containerWidth; }
		      else
		        { 
		         int lines = (int) ((textWidth + containerWidth) / containerWidth);
		                  
		         desiredWidth = (int) (textWidth / lines);
		        }
		                 
		      BreakIterator boundary = BreakIterator.getWordInstance();
		      boundary.setText(text);
		   
		      StringBuffer trial = new StringBuffer();
		      StringBuffer real = new StringBuffer("<html><center>");
		   
		      int start = boundary.first();
		      for (int end = boundary.next(); end != BreakIterator.DONE;
		           start = end, end = boundary.next())
		        {
		         String word = text.substring(start,end);
		         trial.append(word);
		         int trialWidth = SwingUtilities.computeStringWidth(fm,trial.toString());
		         if (trialWidth > containerWidth) 
		           {
		            trial = new StringBuffer(word);
		            real.append("<br>");
		            real.append(word);
		           }
		         else if (trialWidth > desiredWidth)
		           {
		            trial = new StringBuffer("");
		            real.append(word);
		            real.append("<br>");
		           }
		         else
		           { real.append(word); }
		        }
		   
		      real.append("</html>");
		   
		      label.setText(real.toString());
		     }

	public static void main(String[] args) {
		 SwingUtilities.invokeLater(
			        new Runnable() 
			          {  
			           public void run() { new DiagnozowanieStomatologiczne(); }  
			          });   

	}

}
